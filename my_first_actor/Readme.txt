Have a GUI that acts as an instrument controller that can control any number of instruments.   These instruments can be a specAn with random data, DMM with random data, DIO with random data.
All Controlled inside subpanels in the top level actor.

The instruments can be supported as subpanels, but also pop in and out of the subpanel control.   

The nested actors can send random data up to the top subpanel as waveforms or measurements or whatever. 